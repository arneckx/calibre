/*
 * action types
 */
export const TOGGLE_BOOK = 'TOGGLE_BOOK';

/*
 * action creators
 */
export const toggleBook = book => ({
    type: TOGGLE_BOOK,
    book
});
