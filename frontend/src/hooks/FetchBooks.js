// eslint-disable-next-line no-unused-vars
import React, {useEffect, useState} from "react";

const FetchBooks = (searchText) => {
    const [error, setError] = useState("");
    const [isLoaded, setIsLoaded] = useState(false);
    const [books, setBooks] = useState([]);
    const [offset, setOffset] = useState(0);
    const count = 40;

    useEffect(() => {
        setBooks([]);
        if (offset != 0) {
            setOffset(0);
        } else {
            fetchBooks(true);
        }
    }, [searchText]);


    useEffect(() => {
        fetchBooks(false)
    }, [offset]);

    function increaseOffset() {
        setOffset(offset + count);
    }


    function fetchBooks(reinitialize) {
        fetch(window.CALIBRE_API_URL + "/library?searchtext=" + searchText + "&count=" + count + "&offset=" + offset)
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    if (reinitialize) {
                        setBooks(result);
                    } else {
                        setBooks(books.concat(result));
                    }
                },
                (err) => {
                    setIsLoaded(true);
                    setError(err)
                }
            )
    }

    return [error, isLoaded, books, increaseOffset];
};

export default FetchBooks;
