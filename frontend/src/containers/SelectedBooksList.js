import {connect} from 'react-redux'
import {toggleBook} from '../actions/actions'
import SelectedBooks from "../components/SelectedBooks";

const getSelectedBooks = (todos) => {
    return todos
};

const mapStateToProps = state => ({
    selectedBooks: getSelectedBooks(state.selectedBooks)
});

const mapDispatchToProps = dispatch => ({
    toggleBook: book => dispatch(toggleBook(book))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SelectedBooks)
