import React, {useState} from 'react';
import './App.css';
import './App.sass';
import BookList from "./components/BookList";
import SearchBar from "./components/SearchBar";
import SelectedBooksList from "./containers/SelectedBooksList";

function App() {
    const [searchText, setSearchText] = useState("");

    return (

        <div className={"App"}>
            <div className={"section"}>
                <h1 className={"title is-1"}>Calibre</h1>
            </div>
            <div className={"section"}>
                <SearchBar searchText={searchText} setSearchText={setSearchText}/>
            </div>
            <div className={""}>
                <SelectedBooksList/>
            </div>
            <div className={"section"}>
                <BookList searchText={searchText}/>
            </div>
        </div>

    );
}

export default App;
