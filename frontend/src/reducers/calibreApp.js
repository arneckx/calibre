import {combineReducers} from 'redux'
import {TOGGLE_BOOK} from "../actions/actions";

function selectedBooks(state = [], action) {

    switch (action.type) {
        case TOGGLE_BOOK:
            return toggle();

        default:
            return state;
    }

    function toggle() {
        let index = state.indexOf(action.book);
        if (index >= 0) {
            return state.filter(book => book.uuid !== action.book.uuid);

        } else {
            return [...state, action.book];
        }
    }
}

const calibreApp = combineReducers({
    selectedBooks
});

export default calibreApp
