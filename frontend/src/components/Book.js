import * as React from "react";
import {useState} from "react";
import {toggleBook} from "../actions/actions";
import {connect} from "react-redux";

const Book = ({book, dispatch}) => {
    const [isClicked, setClicked] = useState(false);
    return (
        <div className={isClicked ? 'clicked' : 'not-clicked'}>
            <div key={book.uuid} className={"card"}
                 onClick={(e) => {
                     setClicked(!isClicked);
                     e.preventDefault();
                     dispatch(toggleBook(book))
                 }}>
                <div className="card-image">
                    <figure className="image is-2by3">
                        <img src={window.CALIBRE_API_URL + "/image?bookUuid=" + book.uuid} alt={"book cover"}/>
                    </figure>
                </div>
                <div className="card-content">
                    <div className="content">
                        <h2>{book.information}</h2>
                    </div>
                </div>
            </div>
        </div>);
};

export default connect()(Book)
