import * as React from "react";
import Book from "./Book"
import FetchBooks from "../hooks/FetchBooks";

export default function BookList({searchText}) {
    const [error, isLoaded, books, increaseOffset] = FetchBooks(searchText);

    if (error) {
        return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
        return <div>Loading...</div>;
    } else {
        return (
            <div className={"container"}>
                {books.map(book => {
                    return <Book key={book.uuid} book={book}/>
                })}
                <div className="control">
                    <button key={"download"} className="button is-link"
                            onClick={() => {increaseOffset();}}>Get next books
                    </button>
                </div>
            </div>
        );
    }
}
