import * as React from "react";

export default function SearchBar({searchText, setSearchText}) {
    return (
        <div className={"container"}>
            <div className={"searchbar field"}>
                <label className="label">Search</label>
                <div className="control">
                    <input className="input" type="text" placeholder="e.g Stephen King" value={searchText}
                           onChange={val => setSearchText(val.target.value)}/>
                </div>
            </div>
        </div>
    );
}

