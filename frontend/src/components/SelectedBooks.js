import * as React from "react";


const SelectedBooks = ({selectedBooks}) => (
    <div key={"selectedBooks"} className={"selectedBooks card"}>
        <div className="card-content">
            <h1 className={"title is-4"}>Selected books</h1>
            <div className="content is-scrollable">
                <ul>
                    {selectedBooks.map(book => {
                        return <li key={book.uuid}>{book.information}</li>
                    })}
                </ul>
            </div>

            <div className="field is-grouped is-grouped-centered">
                <div className="control">
                    <button key={"download"} className="button is-link"
                            onClick={() => downloadBooks(selectedBooks)}>Download
                    </button>
                </div>
            </div>
        </div>
    </div>

);

function downloadBooks(books) {
    const headers = {
        "Content-Type": "application/json",
        "Access-Control-Origin": "*"
    };

    fetch(window.CALIBRE_API_URL + "/books", {
        method: "POST",
        body: JSON.stringify(books),
        headers: headers
    }).then((response) => {
        response.blob().then(blob => {
            let url = window.URL.createObjectURL(blob);
            let a = document.createElement('a');
            a.href = url;
            a.download = "calibri.zip";
            a.click();
        });
    }).catch((error) => {
        console.log(error);
    });
}

export default SelectedBooks


