package arneckx.calibri.controller;

import arneckx.calibri.common.exception.CalibriException;
import arneckx.calibri.dto.Book;
import arneckx.calibri.service.LibraryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.UrlResource;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.zip.*;

@Controller
@Slf4j
public class BookController {
	@Autowired
	private LibraryService libraryService;
	@Autowired
	private ServletContext servletContext;

	@GetMapping(path = "image")
	@ResponseBody
	public ResponseEntity getImageAsResource(@RequestParam(name = "bookUuid") String bookUuid) throws MalformedURLException {
		return ResponseEntity.ok()
							 .contentType(MediaType.IMAGE_JPEG)
							 .body(new UrlResource(Paths.get(libraryService.getLibrary()
																		   .get(bookUuid)
																		   .getImageUrl()
																		   .toString())
														.toUri()));
	}

	@CrossOrigin
	@GetMapping(path = "library", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Book>> getLibrary(@RequestParam(name = "count", defaultValue = "20", required = false) int count,
													   @RequestParam(name = "offset", defaultValue = "0", required = false) int offset,
													   @RequestParam(name = "searchtext", required = false) String searchText) {
		return ResponseEntity.ok(libraryService.getLibrary()
											   .values()
											   .stream()
											   .filter(getSearchPredicate(searchText))
											   .skip(offset)
											   .limit(count)
											   .collect(Collectors.toList()));
	}

	@CrossOrigin
	@PostMapping(path = "books", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity downloadBooks(@RequestBody Collection<Book> books) {
		String zipName = "calibre.zip";
		try (FileOutputStream fos = new FileOutputStream(zipName);
			 ZipOutputStream zipOut = new ZipOutputStream(fos)) {
			for (Book book : books) {
				Book bookToDownload = fetchFromLibrary(book);
				zipFile(bookToDownload.getParentDirectory()
									  .toFile(), bookToDownload.getParentDirectory()
															   .getFileName()
															   .toString(), zipOut);
			}

			return ResponseEntity.ok()
								 .contentType(MediaType.parseMediaType("application/zip"))
								 .body(new UrlResource(Paths.get(zipName)
															.toUri()));
		} catch (IOException e) {
			throw new CalibriException(e);
		}
	}

	private Book fetchFromLibrary(Book book) {
		return libraryService.getLibrary()
							 .get(book.getUuid());
	}

	private static void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {
		if (fileToZip.isHidden()) {
			return;
		}
		if (fileToZip.isDirectory()) {
			if (fileName.endsWith("/")) {
				zipOut.putNextEntry(new ZipEntry(fileName));
				zipOut.closeEntry();
			} else {
				zipOut.putNextEntry(new ZipEntry(fileName + "/"));
				zipOut.closeEntry();
			}
			File[] children = fileToZip.listFiles();
			for (File childFile : children) {
				zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);
			}
			return;
		}
		try (FileInputStream fis = new FileInputStream(fileToZip)) {
			ZipEntry zipEntry = new ZipEntry(fileName);
			zipOut.putNextEntry(zipEntry);
			byte[] bytes = new byte[1024];
			int length;
			while ((length = fis.read(bytes)) >= 0) {
				zipOut.write(bytes, 0, length);
			}
		}
	}

	private Predicate<Book> getSearchPredicate(String searchText) {
		return b -> searchText == null || (b.getInformation() != null && b.getInformation()
																		  .toLowerCase()
																		  .contains(searchText.toLowerCase()));
	}
}
