package arneckx.calibri.common.exception;

public class CalibriException extends RuntimeException {

	public CalibriException(String message) {
		super(message);
	}

	public CalibriException(String message, Throwable cause) {
		super(message, cause);
	}

	public CalibriException(Throwable cause) {
		super(cause);
	}
}
