package arneckx.calibri.service;

import arneckx.calibri.dto.Book;

import java.util.*;

public interface LibraryService {
	Map<String,Book> getLibrary();
}
