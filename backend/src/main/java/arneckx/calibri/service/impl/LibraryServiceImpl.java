package arneckx.calibri.service.impl;

import arneckx.calibri.common.exception.CalibriException;
import arneckx.calibri.dto.Book;
import arneckx.calibri.service.LibraryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.stream.Stream;

@Service
@Slf4j
public class LibraryServiceImpl implements LibraryService {
	private static Map<String, Book> library = new LinkedHashMap<>();

	@Value("${library}")
	private String libraryLocation;

	@Override
	public synchronized Map<String, Book> getLibrary() {
		if (library.isEmpty()) {
			fetchLibrary(libraryLocation);
		}
		return new LinkedHashMap<>(library);
	}

	private void fetchLibrary(String libraryLocation) {
		try (Stream<Path> filesStream = Files.walk(Paths.get(libraryLocation), 10)) {
			HashMap<Path, Book> bookHashMap = new HashMap<>();

			filesStream.forEach(path -> {
				if (Files.isRegularFile(path)) {
					if (fileExtensionIs(path, ".epub")) {
						bookHashMap.put(path.getParent(), getBook(bookHashMap, path).setInformation(path.getFileName()
																										.toString())
																					.setParentDirectory(path.getParent()
																											.normalize()
																											.toAbsolutePath()));
					}
					if (fileExtensionIs(path, ".jpg")) {
						bookHashMap.put(path.getParent(), getBook(bookHashMap, path).setImageUrl(path.normalize()
																									 .toAbsolutePath()));
					}
				}
			});

			bookHashMap.values()
					   .stream()
					   .sorted(getBookComparator())
					   .forEach(book -> library.put(book.getUuid(), book));
			log.info("fetched library: " + libraryLocation);
		} catch (IOException e) {
			throw new CalibriException(e);
		}
	}

	private boolean fileExtensionIs(Path path, String s) {
		return path.getFileName()
				   .toString()
				   .endsWith(s);
	}

	private Comparator<Book> getBookComparator() {
		return (b1, b2) -> {
			if (b1.getInformation() != null && b2.getInformation() != null) {
				return b1.getInformation()
						 .compareTo(b2.getInformation());
			}
			return b1.getUuid()
					 .compareTo(b2.getUuid());
		};
	}

	private Book getBook(HashMap<Path, Book> bookHashMap, Path path) {
		Book book = bookHashMap.get(path.getParent());
		if (book == null) {
			book = new Book();
		}
		return book;
	}
}
