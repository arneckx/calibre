package arneckx.calibri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalibriApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalibriApplication.class, args);
	}
}
