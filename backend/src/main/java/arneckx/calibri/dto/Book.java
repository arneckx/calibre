package arneckx.calibri.dto;

import arneckx.calibri.common.exception.CalibriException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.nio.file.Path;
import java.util.UUID;

@Data
@Accessors(chain = true)
public class Book implements Serializable {
	private String uuid = UUID.randomUUID()
							  .toString();
	private String information;
	@JsonIgnore
	private transient Path imageUrl;
	@JsonIgnore
	private transient Path parentDirectory;
}
