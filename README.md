# Calibre
PLayground project for learning Kubernetes with microk8s

## Requirements
- Docker
- microk8s (kubernetes)

## Setup
Create lib directory in project.\
Add your book library to lib directory.\
Change the backend/src/main/resources/application.properties file with your library name.

### dockerize Calibre backend
sudo docker build --tag=calibre-backend -f ./backend/Dockerfile .\
sudo docker save calibre-backend > calibre-backend.tar\
sudo microk8s.ctr image import calibre-backend.tar

### dockerize Calibre frontend
go into frontend directory\
docker build -f Dockerfile-prod -t calibre-frontend:prod .\
sudo docker save calibre-frontend:prod > calibre-frontend.tar\
sudo microk8s.ctr image import calibre-frontend.tar

#### change the environment variables for the react app
https://dev.to/ama/react-app-deployment-on-kubernetes-with-kubectl-kustomize-and-helm-in-a-multi-environment-setup-5b1o\
mctl create configmap calibre-app-config --from-file=config.js=./env/kubernetes.properties

### change host file
add entries for arneckx.calibre.backend arneckx.calibre:\
127.0.0.1	localhost arneckx.calibre.backend arneckx.calibre

## Startup
mctl apply -f ./calibre-backend-kubernetes.yaml

go to http://arneckx.calibre
